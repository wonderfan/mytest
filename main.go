package main

import (
	"flag"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/mytest/pkg"
)

const letters = "abcdefghijklmnopqrstuvwxyz"

func main() {
	valid := flag.Bool("valid", true, "valid flag")
	length := flag.Int("length", 3, "word number")
	flag.Parse()
	result := generate(*valid, *length)
	fmt.Println("value: ", result)
	strOps := pkg.NewStrOps(result)
	if *valid {
		fmt.Println("valid: ", strOps.Validate())
		fmt.Println("average: ", strOps.Average())
		fmt.Println("story: ", strOps.Story())
		min, max, avg, list := strOps.Statistics()
		fmt.Println("statistics: ", min, max, avg, list)
	} else {
		fmt.Println("valid: ", strOps.Validate())
	}
}

func generate(valid bool, length int) string {
	rand.Seed(time.Now().UnixNano())
	result := ""
	for i := 0; i < length; i++ {
		if i == length-1 {
			result = result + strconv.Itoa(rand.Intn(99)+1) + "-" + randstr(rand.Intn(len(letters)))
		} else {
			result = result + strconv.Itoa(rand.Intn(99)+1) + "-" + randstr(rand.Intn(len(letters))) + "-"
		}

	}
	if !valid {
		result = randstr(2) + result
	}
	return result
}

func randstr(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
