package pkg

import (
	"math"
	"regexp"
	"strconv"
	"strings"
)

type StrI interface {
	Validate() bool
	Average() int
	Story() string
	Statistics() string
}

type StrOps struct {
	Input string
}

func NewStrOps(str string) *StrOps {
	return &StrOps{
		Input: str,
	}
}

func (strOps *StrOps) Validate() bool {
	exp := regexp.MustCompile(`^(\d+-[a-z]+)-.+-(\d+-[a-z]+)$`)
	return exp.MatchString(strOps.Input)
}

func (strOps *StrOps) Average() float64 {
	str := strings.Split(strOps.Input, "-")
	var list []int
	for i := 0; i < len(str); i++ {
		if i%2 == 0 {
			v, err := strconv.Atoi(str[i])
			if err == nil {
				list = append(list, v)
			}

		}
	}
	sum := 0
	size := len(list)
	for k := 0; k < size; k++ {
		sum = sum + list[k]
	}
	return float64(sum) / float64(size)
}

func (strOps *StrOps) Story() string {
	str := strings.Split(strOps.Input, "-")
	var story []string
	for i := 0; i < len(str); i++ {
		if i%2 == 1 {
			story = append(story, str[i])
		}
	}
	return strings.Join(story, " ")
}

func (strOps *StrOps) Statistics() (int, int, float64, []string) {
	str := strings.Split(strOps.Input, "-")
	var texts []string
	for i := 0; i < len(str); i++ {
		if i%2 == 1 {
			texts = append(texts, str[i])
		}
	}
	min := len(texts[0])
	max := 0
	sum := 0
	for _, v := range texts {
		length := len(v)
		if min > length {
			min = length
		}
		if length > max {
			max = length
		}
		sum = sum + length
	}
	size := len(texts)
	avg := float64(sum) / float64(size)
	var list []string
	for j := 0; j < size; j++ {
		ln := len(texts[j])
		if ln == int(math.Ceil(avg)) || ln == int(math.Floor(avg)) {
			list = append(list, texts[j])
		}
	}

	return min, max, avg, list
}
