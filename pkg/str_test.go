package pkg

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func test_story(t *testing.T) {
	sample := "23-ab-48-caba-56-haha"
	strOps := NewStrOps(sample)
	assert.NotEmpty(t, strOps.Story())
}
